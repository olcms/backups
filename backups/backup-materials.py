# coding=utf-8

import shell
import pathlib

import utils
from utils import run_command

COMMAND_TEMPLATE = 'swift download --output-dir {output} --skip-identical {container}'

def download_swift(output_dir: str, container: str):

  command = COMMAND_TEMPLATE.format(
    output = str(output_dir),
    container = container,
  )

  print(command)
  run_command(command)


def synchronize(swift_path, borg_path, container):
  download_swift(swift_path, container)
  utils.add_borg_archive(borg_path, swift_path)


synchronize(utils.MATERIALS_SWIFT_PREPROD, utils.BORG_SWIFT_PREPROD, 'preprod')
synchronize(utils.MATERIALS_SWIFT_PROD, utils.BORG_SWIFT_PROD, 'olcmsProd')
synchronize(utils.SQL_ROOT, utils.BORG_SWIFT_SQL, 'db-backups')