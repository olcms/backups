# coding=utf-8
import datetime
import pathlib
import os
import subprocess
import sys
import shell
import shlex

def run_command(cmd):
  print(subprocess.check_output(shlex.split(cmd)))

BACKUP_ROOT = pathlib.Path(os.environ['BACKUP_LOCAL_ROOT'])

SWIFT_ROOT = BACKUP_ROOT / 'swift'

MATERIALS_SWIFT = SWIFT_ROOT / 'materials'

MATERIALS_SWIFT_PREPROD = MATERIALS_SWIFT / 'preprod'
MATERIALS_SWIFT_PREPROD.mkdir(exist_ok=True, parents=True)

MATERIALS_SWIFT_PROD = MATERIALS_SWIFT / 'prod'
MATERIALS_SWIFT_PROD.mkdir(exist_ok=True, parents=True)

BORG_ROOT = BACKUP_ROOT / 'borg'

BORG_SWIFT_PREPROD = BORG_ROOT / 'swift-preprod'
BORG_SWIFT_PREPROD.mkdir(exist_ok=True, parents=True)

BORG_SWIFT_PROD = BORG_ROOT / 'swift-prod'
BORG_SWIFT_PREPROD.mkdir(exist_ok=True, parents=True)

SQL_ROOT = BACKUP_ROOT / 'sql'
SQL_ROOT.mkdir(exist_ok=True, parents=True)

BORG_SWIFT_SQL = BORG_ROOT / 'sql'
BORG_SWIFT_SQL.mkdir(exist_ok=True, parents=True)


CREATE_BORG_BACKUP_DIR = 'borg init -e none {path}'


def create_borg_repo(path):
  run_command(CREATE_BORG_BACKUP_DIR.format(path=path))


ADD_BORG_ARCHIVE_PATH = 'borg create {repo}::{archive_name} {backup_path}'


def add_borg_archive(repo_path, backup_path):

  archive_name = datetime.datetime.now().isoformat().replace(':', '-')

  cmd = ADD_BORG_ARCHIVE_PATH.format(
    repo=repo_path, archive_name=archive_name, backup_path=backup_path
  )

  run_command(cmd)
