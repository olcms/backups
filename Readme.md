Backup scripts. 

Works as follows: 

It downloads swift container, and then creates a borg snapshot 
of this container. 

Borg is deduplicated backup tool, even if data will be deleted
from container data will be recoverable, and since we use borg
we won't store too much data. 
   
Following containers are backed up: 

* ``preprod`` --- files from preprod enviornemnt
* ``olcmsProd`` --- production files 
* ``db-backups`` --- sql backups

You'll need following variables in your enviornemnt:
        
    ST_USER=...
    ST_KEY=...
    ST_AUTH=https://ocs-pl.oktawave.com/auth/v1.0
    
    BACKUP_LOCAL_ROOT=/backups

